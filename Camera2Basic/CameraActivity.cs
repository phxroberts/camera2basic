﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V13.App;

namespace Camera2Basic
{
	[Activity (Label = "Camera2Basic", MainLauncher = true, Icon = "@drawable/icon")]
	public class CameraActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			ActionBar.Hide ();
			SetContentView (Resource.Layout.activity_camera);

			if (bundle == null) {
				FragmentManager.BeginTransaction ().Replace (Resource.Id.container, Camera2BasicFragment.NewInstance ()).Commit ();
			}
			hasStoragePermission();
		}


		public bool hasStoragePermission()
		{
			if ((int)Build.VERSION.SdkInt >= 23)
			{
				if (CheckSelfPermission(Android.Manifest.Permission.WriteExternalStorage) == Android.Content.PM.Permission.Granted)
				{
					Console.WriteLine("Permission error", "You have permission");
					return true;
				}
				else
				{

					Console.WriteLine("Permission error", "You have asked for permission");
					ActivityCompat.RequestPermissions(this, new String[] { Android.Manifest.Permission.WriteExternalStorage }, 1);
					return false;


				}
			}
			else
			{ //you dont need to worry about these stuff below api level 23
				Console.WriteLine("Permission error", "You already have the permission");
				return true;
			}
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
		{
			base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}

	}
}




using System;
using Android.Content;
using Android.Graphics;
using Android.Media;
using Java.IO;
using Java.Lang;
using Java.Nio;

namespace Camera2Basic.Listeners
{
    public class ImageAvailableListener : Java.Lang.Object, ImageReader.IOnImageAvailableListener
    {
		string m_FileName = "pic";

        public File File { get; private set; }
        public Camera2BasicFragment Owner { get; set; }
		public Context Context { get; set; }
		public class ImageSavedEventArgs : EventArgs
		{
			public string FilePath { get; set; }
		}
		public delegate void ImageSavedEventHandler(object sender, ImageSavedEventArgs e);
		public event ImageSavedEventHandler ImageSaved = delegate { };

		public ImageAvailableListener()
		{
			createFile();
		}
        public void OnImageAvailable(ImageReader reader)
        {
			createFile();
			Owner.mBackgroundHandler.Post(new ImageSaver(this, this.Context, reader.AcquireNextImage(), File));
        }
		public void RaiseImageSavedEvent(ImageSavedEventArgs args)
		{
			ImageSaved(this, args);
		}
		void createFile()
		{
			string basePath = Android.OS.Environment.DirectoryPictures;
			string picFilelLocation = System.IO.Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(basePath).AbsolutePath, m_FileName + ".jpg");
			int iFileCount = 0;
			while (new System.IO.FileInfo(picFilelLocation).Exists)
			{
				picFilelLocation = System.IO.Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(basePath).AbsolutePath, m_FileName + (++iFileCount).ToString() + ".jpg");
			}
			File = new File(picFilelLocation);
		}


		private class ImageSaver : Java.Lang.Object, IRunnable, MediaScannerConnection.IOnScanCompletedListener
		{
			// The JPEG image
			private Image mImage;
			// The file we save the image into.
			private File mFile;
			Context m_Context;
			ImageAvailableListener m_ImageAvailableListener;

			public ImageSaver(ImageAvailableListener imageAvailableListener, Context contextInstance, Image image, File file)
			{
				m_ImageAvailableListener = imageAvailableListener;
				m_Context = contextInstance;
				mImage = image;
				mFile = file;
			}

			public void Run()
			{
				ByteBuffer buffer = mImage.GetPlanes()[0].Buffer;
				byte[] bytes = new byte[buffer.Remaining()];
				buffer.Get(bytes);
				using (var output = new FileOutputStream(mFile))
				{
					try
					{
						output.Write(bytes);
					}
					catch (IOException e)
					{
						e.PrintStackTrace();
					}
					finally
					{
						mImage.Close();
					}
				}
				string filePath = timestampItAndSave(mFile, false);
				MediaScannerConnection.ScanFile(m_Context, new string[] { filePath }, null, this);
				//MediaScannerConnection.ScanFile(m_Context, new string[] { timestampItAndSave(mFile) }, null, this);
				m_ImageAvailableListener.RaiseImageSavedEvent(new ImageSavedEventArgs
				{
					FilePath = filePath
				});
			}

			public void OnScanCompleted(string path, Android.Net.Uri uri)
			{
				System.Diagnostics.Debug.WriteLine("Media Scan Completed");
			}

			string timestampItAndSave(File outputMediaFile, bool bOverwriteOriginal = false)
			{
				string timeStampedFilePath = outputMediaFile.ToString();
				if (bOverwriteOriginal == false)
				{
					System.IO.FileInfo fi = new System.IO.FileInfo(outputMediaFile.ToString());
					timeStampedFilePath = fi.FullName.Replace(fi.Extension, "");
					timeStampedFilePath = timeStampedFilePath + ".ts" + fi.Extension;
				}
				BitmapFactory.Options options = new BitmapFactory.Options();

				options.InPreferredConfig = Bitmap.Config.Argb8888;
				Bitmap bitmap = BitmapFactory.DecodeFile(outputMediaFile.AbsolutePath);

				Bitmap dest = Bitmap.CreateBitmap(bitmap.Width, bitmap.Height, Bitmap.Config.Argb8888);

				string dateTime = System.DateTime.Now.ToString("G");

				Canvas cs = new Canvas(dest);
				Paint tPaint = new Paint();
				tPaint.TextSize = 150;
				tPaint.Color = Color.Red;
				tPaint.SetStyle(Paint.Style.Fill);
				cs.DrawBitmap(bitmap, 0f, 0f, null);
				float height = tPaint.MeasureText("yY");
				cs.DrawText(dateTime, 20f, height + 15f, tPaint);
				try
				{
					using (System.IO.StreamWriter sw = new System.IO.StreamWriter(timeStampedFilePath))
					{
						dest.Compress(Bitmap.CompressFormat.Jpeg, 100, sw.BaseStream);
					}
				}
				catch (FileNotFoundException e)
				{
					// TODO Auto-generated catch block
					e.PrintStackTrace();
				}
				return timeStampedFilePath;
			}
		}

    }
}
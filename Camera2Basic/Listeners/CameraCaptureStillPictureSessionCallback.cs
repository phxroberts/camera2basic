
using System;
using Android.Hardware.Camera2;
using Android.Media;
using Android.Net;
using Android.Util;

namespace Camera2Basic.Listeners
{
	public class CameraCaptureStillPictureSessionCallback : CameraCaptureSession.CaptureCallback, MediaScannerConnection.IOnScanCompletedListener
    {
        private static readonly string TAG = "CameraCaptureStillPictureSessionCallback";

        public Camera2BasicFragment Owner { get; set; }
		public ImageAvailableListener ImageAvailableListener { get; set; }

        public CameraCaptureStillPictureSessionCallback(Camera2BasicFragment owner, ImageAvailableListener imageAvailableListener)
        {
            Owner = owner;
			ImageAvailableListener = imageAvailableListener;
        }

        public override void OnCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result)
        {
			

            Owner.ShowToast("Saved: " + Owner.mFile);
            Log.Debug(TAG, Owner.mFile.ToString());
            Owner.UnlockFocus();


			string filePath = ImageAvailableListener.File.ToString();
			//MediaScannerConnection.ScanFile(Owner.Activity, new string[] { filePath }, null, this);
        }

		public void OnScanCompleted(string path, Android.Net.Uri uri)
		{
			System.Diagnostics.Debug.WriteLine("Media Scan Completed");
		}
	}
}